const express = require("express");
const md5 = require("md5");
const app = express();
const port = 8000;

const userList = [
];

app.use(express.json());

app.get("/", (req, res) => {
  return res.json({ message: "Silahkan registrasi ke /login" });
});

app.get("/login", (req, res) => {
  return res.json(userList);
});

app.post("/register", (req, res) => {
  const queryUser = req.body;
  // Cek kevalidan user,email,pass
  if (queryUser.username === undefined || queryUser.username === "") {
    res.statusCode = 400;
    return res.json({ message: "Username is invalid" });
  }

  if (queryUser.email === undefined || queryUser.email === "") {
    res.statusCode = 400;
    return res.json({ message: "Email is invalid" });
  }

  if (queryUser.pass === undefined || queryUser.pass === "") {
    res.statusCode = 400;
    return res.json({ message: "Password is invalid" });
  }

  // Cek email yang telah teregis
  const availUser = userList.find((hasilData) => {
    return (
      hasilData.username === queryUser.username ||
      hasilData.email === queryUser.email
    );
  });

  if (availUser) {
    return res.json({ message: "Username or email is exist" });
  }

  userList.push({
    id: userList.length + 1,
    username: queryUser.username,
    email: queryUser.email,
    pass: md5(queryUser.pass),
  });
  res.json({ message: "berhasil menambahkan user" });
});

app.post("/login", (req, res) => {
  const queryUser = req.body;
  const user = userList.find((user) => {
    return (
      (user.username === queryUser.username ||
        user.email === queryUser.email) &&
      user.pass === md5(queryUser.pass)
    );
  });
  if (user === undefined) {
    return res.status(404).json({ message: "Credential Tidak ditemukan" });
  }
  return res.json(user);
});

app.listen(port, () => {
  console.log("App is running on port " + port);
});
