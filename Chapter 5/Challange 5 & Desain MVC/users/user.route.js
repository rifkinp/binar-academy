const express = require('express');
const userRoute = express.Router();
const userController = require('./user.controller')

userRoute.get("/login", userController.dataUser);

userRoute.post("/register", userController.userRegister);

userRoute.post("/login", userController.userLogin);

module.exports = userRoute;
