const md5 = require('md5')
const userList = [];

class userModel {
    // cek all user
    getAllUser = () => {
        return userList
    };
    
    // cek user sudah terdaftar atau tidak
    isUserAvail = (queryUser) => {
        const availUser = userList.find((hasilData) => {
        return (
            hasilData.username === queryUser.username ||
            hasilData.email === queryUser.email
            );
        });

        if (availUser) {
        return true}
        else {return false}
      }

          // push registrasi  
    recordNewData = (queryUser) => {
        userList.push({
        id: userList.length + 1,
        username: queryUser.username,
        email: queryUser.email,
        pass: md5(queryUser.pass),
        })
    };
     
    // Cek Login
    checkUserLogin = (username, email, pass) => {
     const dataUser = userList.find((user) => {
        return ((user.username === username || user.email === email) &&
            user.pass === md5(pass));
        });
    return dataUser;
    }
}

module.exports = new userModel;