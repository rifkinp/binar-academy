const { isUserAvail } = require('./user.model');
const userModel = require('./user.model')


class userController {    

  dataUser = (req, res) => {
    const allUser = userModel.getAllUser();
    return res.json(allUser);
  };

  userRegister = (req, res) => {
    const requestData = req.body;
    // Cek pengisian user,email,pass
    if (requestData.username === undefined || requestData.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username is invalid" });
    }
    
    if (requestData.email === undefined || requestData.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email is invalid" });
    }
    
    if (requestData.pass === undefined || requestData.pass === "") {
      res.statusCode = 400;
        return res.json({ message: "Password is invalid" });
      }
      
    // Cek email yang telah teregis
    const hasilData = userModel.isUserAvail(requestData);
console.log(hasilData)
    if (hasilData) {
      return res.json({ message: "User / Email sudah terdaftar" });
    }

    //   record data kedalam userList
    userModel.recordNewData(requestData);

      return res.json({ message: "registrasi berhasil" });
    }


    // Cek Login
    userLogin = (req, res) => {
        const {username, email, pass} = req.body;
        const hasilLogin = userModel.checkUserLogin(username, email, pass);
      
      if (hasilLogin) {
      return res.json(hasilLogin)
      } else {return res.json({message: 'Credential tidak ditemukan'})}
      }
  }
  
  module.exports = new userController;
  