const { json } = require("express");
const express = require("express");
const app = express();
const md5 = require("md5");
const port = 8000;

const userList = [];

app.use(express.json());

app.get("/", (req, res) => {
  return res.json({ message: "Ini adalah Home" });
});

app.get("/register", (req, res) => {
  return res.json(userList);
});

app.post("/register", (req, res) => {
  const cekRegis = req.body;
  if (cekRegis.username === undefined || cekRegis.username === "") {
    res.statusCode = 400;
    return res.json({ message: "username wajib diisi" });
  }

  if (cekRegis.email === "" || cekRegis.email === undefined) {
    res.statusCode = 400;
    return res.json({ message: "email wajib diisi" });
  }

  if (cekRegis.password === "" || cekRegis.password === undefined) {
    res.statusCode = 400;
    return res.json({ message: "password wajib diisi" });
  }

  const telahRegis = userList.find((cekData) => {
    return (
      cekRegis.username === cekData.username || cekRegis.email === cekData.email
    );
  });

  if (telahRegis) {
    return res.json({ message: "user telah registrasi" });
  }

  userList.push({
    id: userList.length + 1,
    username: cekRegis.username,
    email: cekRegis.email,
    password: md5(cekRegis.password),
  });
  return res.json({ message: "berhasil registrasi user" });
});

app.post("/login", (req, res) => {
  const bodyyy = req.body;
  const userLogin = userList.find((cekLogin) => {
    return (bodyyy.username === cekLogin.username || bodyyy.email === cekLogin.email) && md5(bodyyy.password) === cekLogin.password
});

if (userLogin){return res.json(userLogin)
} else if(userLogin === undefined){
  return res.json({message: 'Credential tidak ditemukan'})
}
});

app.listen(port, () => {
  console.log("App is running on port" + port);
});



// // API login
// app.post("/login", (req, res) => {
//   const bodyyy = req.body;
//   const dataUser = userList.find((data) => {
//     return data.username === bodyyy.username && data.password === md5(bodyyy.password);
//     //return data.username === req.body.username && data.password === md5(req.body.password);
//   });
  
//   console.log(dataUser)

//   // if (dataUser) {
//   //   return res.json(dataUser);
//   // } else {
//   //   return res.json({ message: "Invalid credential" });
//   // }
// });


